# Guest Visit Tracker User Guide
## Adding Guests / Recording Visits
1. Open the Guest Visit Tracker.
2. Click on the **Guest Management** button.
3. Click on the **Guest ID** text field.
4. Use the scanner to scan the barcode on the guest's ID. This should automatically fill in the field.
   - Alternatively, the ID number is also written plainly under the barcode. You may manually type this ID into the field.
5. Click on the **Add guest / record visit** button.

![Steps 1 - 5](Final/AddingGuestsRecordingVisits12345RemovingGuests12345.png)

6. Following this, the following information will be requested from the guest:
   a. Residency status
   b. Graduate status
   c. Graduating year
   d. ID year of issue
7. Be sure to confirm this information with every guest, every time they visit. This information can change even over the course of one semester.
8. Click on the **Submit** button to record the visit and the guest information.

![Steps 6 - 8](Final/AddingGuestsRecordingVisits678.png)

## Removing Guests
1. Open the Guest Visit Tracker.
2. Click on the **Guest Management** button.
3. Click on the **Guest ID** text field.
4. Use the scanner to scan the barcode on the guest's ID. This should automatically fill in the field.
   - Alternatively, the ID number is also written plainly under the barcode. You may manually type this ID into the field.
5. Click on the **Add guest / record visit** button.

![Steps 1 - 5](Final/AddingGuestsRecordingVisits12345RemovingGuests12345.png)

6. Click on the **Remove Guest** button to remove the guest from the system.

![Step 6](Final/RemovingGuests6.png)

## Generating Reports
1. Open the Guest Visit Tracker.
2. Click on the **Reporting** button.
3. Click on the **Start date** text field - the beginning of the date range to obtain data from. You may either enter this date manually, or click on the calendar icon to select a date from a visual calendar.
4. Click on the **End date** text field - the end of the date range to obtain data from. You may either enter this date manually, or click on the calendar icon to select a date from a visual calendar.
5. Click on the **Submit** button to retrieve the report, which will download to your browser.

![Steps 1 - 5](Final/GeneratingReports12345.png)