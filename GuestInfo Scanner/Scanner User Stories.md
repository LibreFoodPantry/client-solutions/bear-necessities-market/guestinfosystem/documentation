# **BNM User Stories**
### GuestInfoSystem Scanner
![GIS Scanner Use Case Diagram](https://gitlab.com/LibreFoodPantry/client-solutions/bear-necessities-market/guestinfosystem/documentation/-/raw/main/GuestInfo%20Scanner/Scanner_GuestInfo_Use_Case_Diagram.jpg?ref_type=heads)
---

- **Scanner Story 1 – Add/Log New Guest**

*As a staff member, I want to be able to issue a BNMID to all new guests who arrive at the market in order to track and reference their visits.*

**Confirmation:**
The staff member issues a new barcode sticker to the guest, and enters this ID with the scanner.
New guest indicates their graduation year, residence status, and undergrad/grad status. 
The staff member enters this information into the Guest log form, in addition to the current year
for Year Issued. 
Following visit submission, the UI will indicate whether the Guest was successfully added or if there was an error. 


- **Scanner Story 2 – Log/Upgrade Returning Guest (Legacy wneID)**

*As a staff member, I want to be able to upgrade the wneID of returning legacy Guests to a BNMID in order to track their visit information using the scanner, without losing any previous records.*

**Confirmation:**
The staff member, upon receiving a legacy market ID card (i.e. 115-24) from a guest, can either collect the card and record the new ID number, or place the barcode sticker directly onto the old card. This ensures that records from the previous system may be linked to the new one. 
The staff member then enters the *new* ID into the field using the scanner. The pertinent info is collected from the guest, and the visit is submitted. The UI will then indicate whether or not the Guest and their visit was successfully logged. 


- **Scanner Story 3 – Log Returning Guest (Current BNMID)**

*As a staff member, I want to be able to log the visits of guests who return to the market with existing BNMID stickers.*

**Confirmation:**
The staff member receives the ID sticker from the guest and scans it, and in turn the info fields in the UI should be auto-populated from the database. Following this, the staff member asks the Guest about info updates and makes any necessary changes to the Guest's records. The staff clicks submit and the guest info/visit is submitted. The UI will then indicate whether or not the Guest and their visit was successfully logged.



