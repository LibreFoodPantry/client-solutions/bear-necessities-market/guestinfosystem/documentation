# Guest Info System Documentation

**Last Updated: 11/11/2024.**

### Project Overview

* **Description** - This project contains details about the Bear Necessities Market's GuestInfoSystem, including design, development processes, and licensing.
### Folders
* **Guestinfo Scanner** - This folder contains the procedures for managing guest visits at the Bear Necessities Market using an ID scanner. This includes issuing new IDs, upgrading legacy IDs to new ones, and logging visits for guests with existing IDs.

* **Testing CRUD Methods** - This folder contains the results of the CRUD methods that were tested, including Get, Post, Put, and Delete.

* **User Guide Images** - This folder contains the User Guide frontend interface that employees will use to manage new and returning guests, as well as the information that is associated with their IDs.

## Status
   GuestInfoFrontend, GuestInfoBackend, and GuestInfoIntegration are all currently active and are in operation.

## Setting up the Development Environment 
**Technologies needed to set up a Development Environment:**

* **VSCode** - VSCode can be downloaded for any operating system using the link provided below.

   [VSCode Setup](https://code.visualstudio.com/download)
* **Docker Desktop** - Docker Desktop can be installed for any operating system using the link provided below.

   [Docker Desktop Setup](https://docs.docker.com/get-started/get-docker/)

* **Git** - Git can be downloaded for any operating system using the link provided below.

    [Git Setup](https://git-scm.com/downloads)

**Instructions to set up a Development Environment:**

1.	Start Docker Desktop and keep it open in the background.
2.	Set up VSCode Extensions.
      * Open VSCode.
      * Use Ctrl-Shift-X to open the Extensions panel in Visual Studio Code.
      * Type `Dev Containers` in the search bar.
      * Install the Dev Containers extension by Microsoft ([Link](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-containers))
      * Once installed, close VSCode.
 
 **Installing and running the project and its Dev Containers**
 
   * Naviagte the project you will be working on
   * Instructions for how to install and run the specific project can be found in the `Setting up the Development Environment` section of the README.md file within each project linked below:

      * [GuestInfoFrontend](https://gitlab.com/LibreFoodPantry/client-solutions/bear-necessities-market/guestinfosystem/guestinfofrontend/-/blob/main/README.md?ref_type=heads)
      * [GuestInfoBackend](https://gitlab.com/LibreFoodPantry/client-solutions/bear-necessities-market/guestinfosystem/guestinfobackend/-/blob/main/README.md?ref_type=heads)
      * [GuestInfoIntegration](https://gitlab.com/LibreFoodPantry/client-solutions/bear-necessities-market/guestinfosystem/guestinfointegration/-/blob/main/README.md?ref_type=heads)
      
   
   
 



## Development Infrastructure
All of the tools used in this project can be found in the 
[GuestInfoFrontend](https://gitlab.com/LibreFoodPantry/client-solutions/bear-necessities-market/guestinfosystem/guestinfofrontend/-/blob/main/README.md?ref_type=heads) and 
[GuestInfoBackend](https://gitlab.com/LibreFoodPantry/client-solutions/bear-necessities-market/guestinfosystem/guestinfobackend/-/blob/main/README.md?ref_type=heads) README.MD files.

## License Agreement

This work is licensed under [GPL v3](https://gitlab.com/LibreFoodPantry/client-solutions/bear-necessities-market/guestinfosystem/guestinfobackend/-/blob/main/LICENSE).
